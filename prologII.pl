%seznam [A,B,C]
%[A|B] A- první prvek seznamu
% CTRL+P přechozí a CTRL+N další - pohybovani v prikazové radce
% cislo seznam obasuje prvek Member

%member(+E, +Lst)
my_member(E, [E|_]).
my_member(E,[X|Xs]) :- E\=X,member(E,Xs).

puzzle(N) :-
    Digits= [0,1,2,3,4,5,6,7,8,9],
    member(A,Digits),
    A \= 0,
    member(B,Digits),
    member(C,Digits),
    member(D,Digits),
    member(E,Digits),
    E is A + B + C,
    C is E - 4,
    A + D =:= E - 1,
    E  is 3*C,
    N is 10000*A + 1000*B + 100*C + 10*D + E.
%------------------------------------------------------

min(X,Y,X):- X<Y.
min(X,Y,Y):- X>=Y.
%------------------------------------------------------
%find min
findMin([A],A).
findMin([X|Xs], Min):- findMin(Xs, R), min(X, R, Min).
%-----------------------------------------------------
%FindAll(promena, dotaz s promenou, výsledna promena)

%-----------------------------------------------------
%del
%del(Lst, E, R).
del([],_,[]).
del([X|Xs],X,Xs):- !. %nemusí tam být ten řez ( ! )
del([X|Xs],E, [X|R]) :- E\=X,del(Xs, E, R).

%----------------------------------------------------
%Cvičení 3
my_fact(0,1):-!.
my_fact(N,R):-
    N1 is N - 1,
    my_fact(N1,Res),
    R is N*Res.


retList(N, List) :-
    findall(Y, (between(1, N, X), my_fact(X,Y)), List).
% 1.Y: Tento parametr je první argument findall/3 a představuje vzor, kterým jsou shromažďovány výsledky.
%
% 2.(between(1, N, X), my_fact(X, Y)): Toto je cíl (goal), který je vyhodnocen pro každou hodnotu X
%     generovanou pomocí between/3. Tento cíl obsahuje dvě části: první část between(1, N, X) generuje
%     čísla od 1 do N (včetně), a druhá část my_fact(X, Y) volá vaši funkci my_fact/2 pro každé z těchto
%     čísel a přiřazuje výsledek proměnné Y.
%
% 3.List: Třetí parametr findall/3 představuje seznam, do kterého jsou shromažďovány všechny hodnoty Y,
%     které byly získány z vyhodnocení cíle.


%---------------------------------------------------------------------------------------------------------
%Cvičení 4
zero([]).
successor([], [x]).
successor([x|R],[x,x|R]).
add([],T,T).
add([x|T],Y,[x|Res]):- add(T,Y,Res).
mul([], _, []).
mul([x], Num, Num) :- !.
mul([x|T], Num, Res) :- my_mul(T, Num, Res1), my_add(Res1, Num, Res).
%--------------------------------------------------------------------------------------------------------------
%Cvičení 5
count([X|Lst],X,Res):- count(Lst,X,Res1), Res is Res1 + 1.
count([Y|Lst],X,Res):- Y\=X,count(Lst,X,Res1),Res is Res1 + 1.
count([],_,0).

delAll([],_,[]).
delAll([X|Xs],X,R) :- del(Xs,X,R).
delAll([X|Xs],E,[X|R]):- E\=X, del(Xs,E,R).

counter([],[]).
counter([X|Lst],[[X,C]|Res]):- count([X|Lst],X,C),
                               delAll(Lst,X,Lst1),
                               counter(Lst1, Res).
%-------------------------------------------------------------------------------------------------------------------
%Cvičení 6
%split(+Lst,-L1,-L2)

my_lenght([],0):-!.
my_lenght([_|Lst],Res):- my_lenght(Lst,Res1),Res is Res1 + 1.

split_tail(Lst,0,Lst).
split_tail([X|Lst],N,Res):-X\=[], N1 is N-1, split_tail(Lst,N1,Res).
split_head(_,0,[]).
split_head([X|Lst],N,[X | Res]):-X\=[], N1 is N-1, split_head(Lst,N1,Res).

split(Lst, L1, L2 ):- my_lenght(Lst,Len), Lenght is Len//2, split_head(Lst,Lenght,L1), split_tail(Lst,Lenght,L2).
merge(A,[],A).
merge([],B,B).
merge([A|As],[B|Bs],[A|R]) :- A =< B, merge(As,[B|Bs], R).
merge([A|As],[B|Bs],[B|R]) :- A > B, merge([A|As],Bs, R).

split_fast(Slow,[],[],Slow).
split_fast(Slow,[_],[],Slow).
split_fast([Slow|Slows],[_,_| Fasts], [Slow| A],B) :- split_fast(Slows,Fasts,A,B).
split_fast(L,A,B):-split_fast(L,L,A,B).

merge_sort([],[]):-!.
merge_sort([A],[A]):-!.
merge_sort(Lst,Res):-
                split(Lst,A,B),
                merge_sort(A,As),
                merge_sort(B,Bs),
                merge(As,Bs,Res).
%-----------------------------------------------------------------------------------------------------------------
%Append

my_append([],A,[A]).
my_append([E|Lst],A,[E|Res]):- my_append(Lst,A,Res).

%-----------------------------------------------------------------------------------------------------------------
%Reverse
my_reverse_acc([],R,R).
my_reverse_acc([E|Lst],Acc,R ):- my_reverse_acc(Lst,[E|Acc],R).
my_reverse(Lst,R) :- my_reverse_acc(Lst,[],R).
%-----------------------------------------------------------------------------------------------------------------
%Padl Goal

not_provable(Goal):- Goal , !, fail.
not_provable(_).