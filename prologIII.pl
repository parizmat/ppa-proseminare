%stromy
bst_find(E,bst(E,_,_)). %Když je prvek co se hleda
bst_find(E,bst(V,L,_)):-E < V, bst_find(E,L). % prohledava Levoustranu
bst_find(E, bst(V,_,R)):- E < V, bst_find(E,R). %prohledava pravou stranu
%-----------------------------------------------------------------------------------------------------------------------

%insert
bst_insert(E, empty, bst(E, empty, empty)). % Základní případ pro prázdný strom
bst_insert(E, bst(E, L, R), bst(E, L, R)). % Vkládání do neprázdného stromu
%Moznosti do jake vetve to insertnout
bst_insert(E, bst(V, L, R), Res):-
    E < V, % vyber vetve.
    bst_insert(E, L, NewL), %Zanoreni Nova vetev se ulozi do NewL
    Res = bst(V, NewL, R). %Kontrola a ukončení aby to nepostupovalo dál na zpět. Nový strom (Res) má aktualizovanou levou podvětev (NewL), pravou podvětev (R) zůstává nezměněná.
bst_insert(E, bst(V, L, R), Res):-
    E > V,
    bst_insert(E, R, NewR),
    Res = bst(V, L, NewR).
%-----------------------------------------------------------------------------------------------------------------------

% inorder
bst_inorder(empty,[]).
bst_inorder(bst(V,L,R), Res ) :-
    bst_inorder(L,LL), %Seradi
    bst_inorder(R,RR),
    append(LL, [V|RR], Res).
%-----------------------------------------------------------------------------------------------------------------------

%nonvar(...) zjistí jestli je to číslo nebo ne
    %tohle je pokud je číslo v jednom z kořenů stromu
bst_member(E, T) :- nonvar(E), bst_find(E,T). % E je číslo a pak rovnou najde to číslo.
%bst_member(E, empty) :- fail.
    %backtracking jake čísla jsou v porenech stromu
bst_member(E,bst(E, _, _)). % E je proměnna? unifikace pokud E je v koreni tak se za E dosadi hodnota a uz to neni promena
bst_member(E, bst(_, L, R)) :- % Budeme prohledavat strom
    bst_member(E,L) %Prohledame po levou stranu stromu
    ; %Pozor zde je or chceme prohledavat oba stromy kdyby tu byl and tak prohledame jen jednu cast.
    bst_member(E,R). %Prohledavame pravou stranu stromu
%-----------------------------------------------------------------------------------------------------------------------

% Přesun z A do B, když je dostupné více, než je obsah A
move(VA/CA, VB/CB, VA/NCA, VB/NCB):-
    Available is VB - CB,   % Vypočítá dostupné místo v B
    CA > Available,         % Pokud je obsah A větší než dostupné místo v B
    NCA is CA - Available,  % Nový obsah A po přesunu
    NCB = VB.               % Nový obsah B po přesunu

% Přesun z A do B, když je dostupné stejně nebo méně, než je obsah A
move(VA/CA, VB/CB, VA/NCA, VB/NCB):-
    Available is VB - CB,   % Vypočítá dostupné místo v B
    CA =< Available,        % Pokud je obsah A menší nebo roven dostupnému místu v B
    NCA is 0,                % Nový obsah A po přesunu (prázdný)
    NCB is CB + CA.         % Nový obsah B po přesunu

% Funkce pro vyzkoušení všech možných pohybů
trymove([A, B, C], [NA, NB, NC]):-
    (move(A, B, NA, NB), NC = C);
    (move(A, C, NA, NC), NB = B);
    (move(B, A, NB, NA), NC = C);
    (move(B, C, NB, NC), NA = A);
    (move(C, A, NC, NA), NB = B);
    (move(C, B, NC, NB), NA = A).

% Funkce pro řešení problému
solve(Start,Visited,Start,Visited).
solve(Start, Visited, Target, Path):-
     trymove(Start, N),           % Vyzkouší všechny možné tahy
     not(member(N,Visited)),      % Pokud nový stav nebyl již navštíven
     solve(N,[N|Visited],Target, Path).

% Alternativní verze funkce pro řešení problému s nalezením nejkratší cesty
shortestList([A],A).
shortestList([X1,X2|Xs],A):-
    shortestList([X2|Xs],R),      % Rekurzivně hledá nejkratší cestu
    length(X1, X1Len),             % Délka aktuální cesty
    length(R, RLen),               % Délka nejkratší cesty
    (
        (X1Len =< RLen, A = X1);   % Pokud je aktuální cesta kratší nebo rovna
        (X1Len >= RLen, A = R)     % Jinak použijeme nejkratší cestu dosud
    ).

% Funkce pro nalezení nejkratší cesty
solveShortest(S,T, Res) :-
    findall(P, solve(S,T,P),L),    % Seznam všech možných cest
    shortestList(L,Res).           % Nalezení nejkratší cesty
%-----------------------------------------------------------------------------------------------------------------------
% Odstranění první instance prvku X z seznamu [X|Xs]
delete_one(X,[X|Xs],Xs).

% Rekurzivní odstranění první instance prvku X z seznamu [E|Xs]
delete_one(X,[E|Xs],[E|R]):-
    delete_one(X,Xs,R).

% Generování všech permutací seznamu Lst
perm([],[]).
perm(Lst, [X| Res]):-
    delete_one(X,Lst,NLst), % Odstranění X z Lst
    perm(NLst,Res).         % Rekurzivní generování permutací z nového seznamu

% Ověření, zda je seznam uspořádaný
issorted([]).
issorted([_]).
issorted([A,B|Rest]):-
    A =< B, issorted([B|Rest]). % Rekurzivní kontrola uspořádanosti

% Debugovací verze třídící funkce
dbgsort(Lst, Res):-
    perm(Lst,Res),      % Vygenerování všech permutací
    issorted(Res),!.    % Ověření, zda jsou permutace uspořádané

% Třídící funkce (quicksort)
srt([],[]).
srt([A],[A]):-!.          % Seznam s jedním prvkem je již uspořádaný
srt(Lst, [X|RestSorted]):-
    delete_one(X,Lst,Rest),    % Odstranění X z Lst
    srt(Rest,RestSorted),      % Rekurzivní třídění zbytku seznamu
    RestSorted=[A|_],           % První prvek setříděného zbytku
    X=<A, !.                    % Ověření, zda X patří před A, pokud ano, skončí

% Vsetavěné sorty
    %sort(R,RR) vyhodí duplicity a msort(R,RR) necha duplicity

% Užitečné funkce:
%               select
%               findall
%               between
